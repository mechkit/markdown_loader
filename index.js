var markdown_specs = require('markdown_specs');

/**
 * .
 * @exports
 */
module.exports = function(markdown_text){
  var specs = markdown_specs(markdown_text);

  return 'module.exports = ' + JSON.stringify(specs);
};
